<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     mod_flashcard
 * @copyright   2021 Manish Kumar & Sara Cousin <manish.kumar@etu.unige.ch>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once(__DIR__ . '/loadDB.php');
require_once(__DIR__ . '/options.php');

/**
 * Return if the plugin supports $feature.
 *
 * @param string $feature Constant representing the feature.
 * @return true | null True if the feature is supported, null otherwise.
 */
function flashcard_supports($feature)
{
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the mod_flashcard into the database.
 *
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 *
 * @param object $data An object from the form.
 * @param mod_flashcard_mod_form $mform The form.
 * @return int The id of the newly inserted record.
 */
function flashcard_add_instance($data, $mform = null)
{
    global $DB;
    global $op_languages;
    global $op_levels;

    //////////////

    $cmid = $data->coursemodule;
    $context = context_module::instance($cmid);
    $fs = get_file_storage();


    $select = "flashcard=" . $cmid;
    $DB->delete_records_select('flashcard_media', $select);

    foreach ($op_languages as $lang) {
        foreach ($op_levels as $level) {
            $name = $lang . '_' . $level;
            $draftitemid = $data->$name;

            if ($draftitemid) {
                $options = array('subdirs' => 0, 'maxfiles' => 50);
                file_save_draft_area_files($draftitemid, $context->id, 'mod_flashcard', $name, 1, $options);
            }

            $files = $fs->get_area_files($context->id, 'mod_flashcard', $name, false, 'filename', false);

            save_files_to_db($files, $cmid, $lang, $level, $name);
        }
    }

    //////////////////////

    $data->timecreated = time();

    $id = $DB->insert_record('flashcard', $data);

    return $id;
}

/**
 * Updates an instance of the mod_flashcard in the database.
 *
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 *
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_flashcard_mod_form $mform The form.
 * @return bool True if successful, false otherwise.
 */
function flashcard_update_instance($data, $mform = null)
{
    global $DB;
    global $op_languages;
    global $op_levels;

    //////////////

    $cmid = $data->coursemodule;
    $context = context_module::instance($cmid);


    $select = "flashcard=" . $cmid;
    $DB->delete_records_select('flashcard_media', $select);

    $fs = get_file_storage();
    foreach ($op_languages as $lang) {
        foreach ($op_levels as $level) {
            $name = $lang . '_' . $level;
            $draftitemid = $data->$name;

            if ($draftitemid) {
                $options = array('subdirs' => 0, 'maxfiles' => 50);
                file_save_draft_area_files($draftitemid, $context->id, 'mod_flashcard', $name, 1, $options);
            }

            $files = $fs->get_area_files($context->id, 'mod_flashcard', $name, false, 'filename', false);

            save_files_to_db($files, $cmid, $lang, $level, $name);
        }
    }

    //////////////////////

    $data->timemodified = time();
    $data->id = $data->instance;

    return $DB->update_record('flashcard', $data);
}

/**
 * Removes an instance of the mod_flashcard from the database.
 *
 * @param int $id Id of the module instance.
 * @return bool True if successful, false on failure.
 */
function flashcard_delete_instance($id)
{
    global $DB;

    $exists = $DB->get_record('flashcard', array('id' => $id));
    if (!$exists) {
        return false;
    }

    $select = "flashcard=" . $id;
    $DB->delete_records_select('flashcard_media', $select);

    $DB->delete_records('flashcard', array('id' => $id));

    return true;
}

/**
 * Extends the global navigation tree by adding mod_flashcard nodes if there is a relevant content.
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $flashcardnode An object representing the navigation tree node.
 * @param stdClass $course.
 * @param stdClass $module.
 * @param cm_info $cm.
 */
function flashcard_extend_navigation($flashcardnode, $course, $module, $cm)
{
}

/**
 * Extends the settings navigation with the mod_flashcard settings.
 *
 * This function is called when the context for the page is a mod_flashcard module.
 * This is not called by AJAX so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@see settings_navigation}
 * @param navigation_node $flashcardnode {@see navigation_node}
 */
function flashcard_extend_settings_navigation($settingsnav, $flashcardnode = null)
{
}

function flashcard_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array())
{

    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'mod_flashcard', $filearea, $args[0], '/', $args[1]);

    send_stored_file($file);
}
