<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The main mod_flashcard configuration form.
 *
 * @package     mod_flashcard
 * @copyright   2021 Manish Kumar & Sara Cousin <manish.kumar@etu.unige.ch>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once(__DIR__ . './options.php');

/**
 * Module instance settings form.
 *
 * @package    mod_flashcard
 * @copyright  2021 Manish Kumar & Sara Cousin <manish.kumar@etu.unige.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_flashcard_mod_form extends moodleform_mod
{

    /**
     * Defines forms elements
     */
    public function definition()
    {
        global $CFG;
        global $op_languages;
        global $op_levels;

        $mform = $this->_form;

        // Adding the "general" fieldset, where all the common settings are shown.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('modulename', 'mod_flashcard'), array('size' => '64'));

        // Adding the standard "intro" and "introformat" fields.
        if ($CFG->branch >= 29) {
            $this->standard_intro_elements();
        } else {
            $this->add_intro_editor();
        }

        // Adding area to input files for each language and each level
        foreach ($op_languages as $lang) {
            $mform->addElement('header', $lang . 'hdr', ucfirst($lang));
            $mform->addElement('html', "<p>You need to upload files in pairs of image file and sound file. The names must be the same. For example : 'apple.png' and 'apple.wav'</p>");
            foreach ($op_levels as $level) {
                $mform->addElement(
                    'filemanager',
                    $lang . '_' . $level,
                    ucfirst($level),
                    null,
                    array(
                        'subdirs' => 0, 'maxbytes' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 50,
                        'accepted_types' => array('audio', 'image')
                    )
                );
            }
        }

        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }

        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'modulename', 'mod_flashcard');


        // Adding the rest of mod_flashcard settings, spreading all them into this fieldset
        // ... or adding more fieldsets ('header' elements) if needed for better logic.
        //$mform->addElement('static', 'label1', 'flashcardsettings', get_string('flashcardsettings', 'mod_flashcard'));
        //$mform->addElement('header', 'flashcardfieldset', get_string('flashcardfieldset', 'mod_flashcard'));

        // Add standard elements.
        $this->standard_coursemodule_elements();

        // Add standard buttons.
        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values)
    {
        global $op_languages;
        global $op_levels;
        foreach ($op_languages as $lang) {
            foreach ($op_levels as $level) {
                $name = $lang . '_' . $level;
                $draftitemid = file_get_submitted_draft_itemid($name);
                file_prepare_draft_area($draftitemid, $this->context->id, 'mod_flashcard', $name, 0, array('subdirs' => true));
                $default_values[$name] = $draftitemid;
            }
        }
    }
}
