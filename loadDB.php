<?php

use core_message\output\messagearea\contact;

require_once(__DIR__ . '/options.php');

function load_samples()
{
    global $DB;

    if (count($DB->get_records("flashcard_media")) > 0) {
        return;
    }

    $location = "pix/pics/";
    $files = get_directory_list(__DIR__ . '/' . $location);

    foreach ($files as $file) {

        $record = new stdClass();
        $record->imagepath = $location . $file;
        $name = explode(".", $file)[0];
        $record->name = $name;
        $record->soundpath = "pix/sounds/" . $name . ".wav";
        $record->language = "german";
        $record->level = "easy";
        $record->flashcard = NULL;

        $DB->insert_record('flashcard_media', $record);
    }
}

function fetch_exo_files($cmid, $context)
{
    global $DB;
    global $op_languages;
    global $op_levels;

    //////////////

    $select = "flashcard=" . $cmid;
    $DB->delete_records_select('flashcard_media', $select);

    $fs = get_file_storage();
    foreach ($op_languages as $lang) {
        foreach ($op_levels as $level) {
            $name = $lang . '_' . $level;
            $files = $fs->get_area_files($context->id, 'mod_flashcard', $name, false, 'filename', false);

            save_files_to_db($files, $cmid, $lang, $level, $name);
        }
    }
}

function save_files_to_db(array $files, $cmid, $lang, $level, $filearea)
{
    global $DB;
    global $CFG;

    $location = "pix/pics/";
    
    foreach ($files as $file) {
        //$file->get_filename()
        //$file->copy_content_to(__DIR__ . '/pix/sounds/' . $file->get_filename());
        $filename = $file->get_filename();
        /* $url = moodle_url::make_file_url('/pluginfile.php', array(
            $file->get_contextid(), 'mod_flashcard', 'content',
            $file->get_itemid(), $file->get_filepath(), $filename
        )); */

        //$url = moodle_url::make_file_url("$CFG->wwwroot/pluginfile.php", "/{$file->get_contextid()}/mod_flashcard/{$lang}/{$file->get_itemid()}/{$filename}");

        $record = new stdClass();
        $type = explode("/", $file->get_mimetype())[0];

        $baseURL = "$CFG->wwwroot/pluginfile.php/{$file->get_contextid()}/mod_flashcard/{$filearea}/{$file->get_itemid()}/{$filename}";
        if ($type == "audio") {
            $record->soundpath = $baseURL;
        } else {
            $record->imagepath = $baseURL;
        }


        $name = explode(".", $filename);

        $record->name = $name[0];
        $record->language = $lang;
        $record->level = $level;
        $record->flashcard = $cmid;

        $query = "SELECT * FROM {flashcard_media} WHERE name='$name[0]' AND language='$lang' AND level='$level'";
        $res = $DB->get_records_sql($query);

        if (empty($res)) {
            $DB->insert_record('flashcard_media', $record);
        } else {
            $record->id = current($res)->id;
            $DB->update_record('flashcard_media', $record);
        }
    }
}
