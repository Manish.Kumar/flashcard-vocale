<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Ribeye&family=Tangerine:wght@700&display=swap" rel="stylesheet">

<div class="fc-main-container" course-id="<?= $id ?>">

    <div class="fc-page-container fc-lang-page">
        <div class="fc-languages">
            <div class="fc-button fc-flashcard fc-lang fc-disabled" id="english">EN</div>
            <div class="fc-button fc-flashcard fc-lang fc-disabled" id="french">FR</div>
            <div class="fc-button fc-flashcard fc-lang fc-disabled" id="italian">IT</div>
            <div class="fc-button fc-flashcard fc-lang fc-disabled" id="spanish">ES</div>
            <div class="fc-button fc-flashcard fc-lang fc-disabled" id="german">DE</div>
        </div>
        <div class="fc-flashcard fc-button fc-main-button" onclick="nextPage(this.parentElement)">
            Start
        </div>
        <!-- <div class="fc-flashcard fc-button fc-main-button">
            Scoreboard
        </div> -->
    </div>

    <div class="fc-page-container fc-level-page fc-move-right">
        <div class="fc-flashcard fc-button fc-secondary-button fc-disabled" id="easy">
            Easy
        </div>
        <div class="fc-flashcard fc-button fc-secondary-button fc-disabled" id="medium">
            Medium
        </div>
        <div class="fc-flashcard fc-button fc-secondary-button fc-disabled" id="hard">
            Hard
        </div>
    </div>

    <div class="fc-page-container fc-exo-page fc-move-right">
        <div class="fc-center fc-banner">
            <img src="pix/banniere.png" alt="banner">
            <div class="fc-banner-title"></div>
        </div>
        <div class="fc-center fc-row fc-card-arrows">
            <img src="pix/arrow.png" alt="microphone" class="fc-arrow fc-left fc-inactive-arrow">
            <div class="fc-cards-container">
                <div class="fc-card fc-flip-card fc-main-card">
                    <div class="fc-flip-card-inner">
                        <div class="fc-flip-card-front fc-flashcard fc-card">
                            <img src="" alt="card" class="fc-image fc-card-image">
                        </div>
                        <div class="fc-flip-card-back fc-flashcard">
                            <div class="fc-back-top">
                                <div class="fc-circle-button fc-image fc-gramophone fc-inactive" onclick="listenAnswer()"></div>
                                <!-- <img src="pix/sound_button_back.png" alt="card" class="fc-image fc-gramophone" onclick="listenAnswer()"> -->
                                <h1 class="fc-answer"></h1>
                                <div class="fc-circle-button fc-image fc-loupe fc-inactive" onclick="toggleDetails()"></div>
                                <!-- <img src="pix/loupe.png" alt="card" class="fc-image fc-loupe" onclick="toggleDetails()"> -->
                            </div>
                            <div class="fc-back-bottom fc-center">
                                <p class="fc-percentage"></p>
                                <div class="fc-details">
                                    <div class="fc-row-space fc-original">
                                        <p class="fc-title" onclick="listenAnswer()">Original :</p>
                                    </div>
                                    <div class="fc-row-space fc-recording">
                                        <p class="fc-title" onclick="listenRecording()">Recording :</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fc-circle-button fc-bottom-circle fc-micro" onclick="record()">
                        <div class="fc-center fc-container-circle">
                            <div class="fc-loader">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <!-- <img src="pix/micro.png" alt="microphone" class="fc-microphone" onclick="record()"> -->
                        </div>
                    </div>
                </div>
            </div>
            <img src="pix/arrow.png" alt="microphone" class="fc-arrow fc-right" onclick="nextExo()">
        </div>
        <div class="fc-center fc-bottom-menu">
            <img src="pix/exit.png" alt="banner" class="fc-exit" onclick="goStartPage()">
            <div class="fc-exos-count"></div>
        </div>
    </div>

</div>

<script src="js/LinguaRecorder/src/AudioRecord.js"></script>
<script src="js/LinguaRecorder/src/LinguaRecorder.js"></script>
<script src="js/lodash/lodash.js"></script>
<script src="//unpkg.com/string-similarity/umd/string-similarity.min.js"></script>
<script src="pages/exo.js"></script>