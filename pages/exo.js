var recorder = null;
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

const languages = {
    EN: 'english',
    FR: 'french',
    IT: 'italian',
    ES: 'spanish',
    DE: 'german'
}
Object.freeze(languages);

const BASLanguages = {
    'english': 'eng',
    'french': 'fra-FR',
    'italian': 'ita-IT',
    'spanish': 'spa-ES',
    'german': 'deu-DE'
}
Object.freeze(BASLanguages);

const googleLanguages = {
    'english': 'en-US',
    'french': 'fr-FR',
    'italian': 'it-IT',
    'spanish': 'es-ES',
    'german': 'de-DE'
}
Object.freeze(BASLanguages);

const levels = {
    EASY: 'easy',
    MEDIUM: 'medium',
    HARD: 'hard',
}
Object.freeze(levels);

const recordState = {
    IDLE: 0,
    RECORDING: 1,
    PROCESSING: 2,
    LOADED: 3,
};
Object.freeze(recordState);

var exos = {
    exosTab: [],
    indexes: [],
    currentIndex: 0,
    nbExos: 0,
    language: languages.EN,
    level: levels.EASY,
    get: function (i) {
        return this.exosTab[this.indexes[i]];
    },
    get current() {
        return this.exosTab[this.indexes[this.currentIndex]];
    },
    get currentInvNumber() {
        return (this.nbExos - 1 - this.currentIndex);
    }
};

var exoHTML = document.getElementsByClassName("fc-main-card")[0].cloneNode(true);
var exoPageHTML = document.getElementsByClassName("fc-exo-page")[0].cloneNode(true);

// Variables storing the previous states of the arrows
var previousLeftArrowClass = null;
var previousRightArrowClass = null;

// Course module ID
var cmid = parseInt(document.getElementsByClassName("fc-main-container")[0].getAttribute("course-id"));

console.log(cmid);

initRecorder();

getLangs();

/////////////// Helpers for fetch


function getJSON(response) {
    return response.json();
}

function getBlob(response) {
    return response.blob();
}

/////////////// Checking available options

function getLangs() {
    let data = {
        cmid: cmid
    }
    let init = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    };

    fetch("pages/serve_lang.php", init)
        .then(checkStatus)
        .then(getJSON)
        .then(langs => {
            let button = null;
            for (const lang in langs) {
                button = document.getElementById(lang);
                button.onclick = function () {
                    setLanguage(lang, this);
                }
                button.classList.remove("fc-disabled");
                console.log(button);
            }
            button.onclick();
        });
}

function getLevels() {
    let data = {
        language: exos.language,
        cmid: cmid
    }
    let init = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    };

    fetch("pages/serve_level.php", init)
        .then(checkStatus)
        .then(getJSON)
        .then(levels => {
            for (const level in levels) {
                let button = document.getElementById(level);
                button.onclick = () => setLevel(level, button);
                button.classList.remove("fc-disabled");
            }
        });
}

/////////////// Navigation and menu selection

function clearExos() {
    let lang = exos.language;
    exos = {
        exosTab: [],
        indexes: [],
        currentIndex: 0,
        nbExos: 0,
        language: lang,
        level: null,
        get: function (i) {
            return this.exosTab[this.indexes[i]];
        },
        get current() {
            return this.exosTab[this.indexes[this.currentIndex]];
        },
        get currentInvNumber() {
            return (this.nbExos - 1 - this.currentIndex);
        }
    };

    let container = document.getElementsByClassName("fc-exo-page")[0];
    [...container.children].forEach(el => el.remove());
    [...exoPageHTML.children].forEach(el => container.append(el.cloneNode(true)));

    for (const level in levels) {
        let button = document.getElementById(levels[level]);
        button.onclick = "";
        button.classList.add("fc-disabled");
    }

}

function setLanguage(lang, el) {
    exos.language = lang;
    let previousSelection = document.getElementsByClassName("fc-selected")[0];
    if (previousSelection)
        previousSelection.classList.remove("fc-selected");
    el.classList.add("fc-selected");
}

function nextPage(el) {
    let nextPage = el.nextElementSibling;
    if (nextPage) {
        nextPage.classList.remove("fc-move-right");
        el.classList.add("fc-move-left");
        if (nextPage.classList.contains("fc-exo-page")) {
            getExos();
        } else if (nextPage.classList.contains("fc-level-page")) {
            console.log("paddddsssededed")
            getLevels();
        }
    } else {
        console.log("last page");
    }
}

function goStartPage() {
    let pages = [...document.getElementsByClassName("fc-page-container")];
    let start = pages.shift();
    pages.forEach(el => {
        el.classList.remove("fc-move-left");
        el.classList.add("fc-move-right");
    })
    start.classList.remove("fc-move-left");
    clearExos();
}

function setLevel(level, el) {
    exos.level = level;
    nextPage(el.parentElement);
}



function getExos() {
    let data = {
        language: exos.language,
        level: exos.level,
        cmid: cmid
    }
    let init = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    };

    fetch("pages/serve_exos.php", init)
        .then(checkStatus)
        .then(getJSON)
        .then((data) => {
            loadData(data);
        });
}


////////////////////// Audio processing


function initRecorder() {
    recorder = new LinguaRecorder();

    recorder.on('ready', function () {
        console.log('ready');
    }).on('started', function () {
        console.log('started');
    }).on('recording', function () {
        console.log('recording');
    }).on('stoped', function (userAudio) {
        console.log('stopped');
        processAudio(userAudio);
    }).on('canceled', function () {
        console.log('cancelled');
    });
}

function processAudio(userAudio) {
    let transcript = null;

    let userData = getTranscription(userAudio.getBlob())
        .then((transcription) => {
            transcript = transcription;
            return loadUserData(userAudio, transcription)
        });

    Promise.all([userData, loadAnswerData()])
        .then(() => {
            updateScore(transcript, exos.current.answerSyllables, exos.current.recordingSyllables)
            document.getElementsByClassName("fc-flip-card")[exos.currentInvNumber].classList.add("fc-rotate");
            recordLoaded();
        });
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response);
    } else {
        return Promise.reject(new Error(response.statusText));
    }
}

function getTranscription(audio) {

    /* let transcription = [
        { "transcript": "Alkohol-Bachgasse", "confidence": 0.8257754445075989 },
        { "transcript": "alkohol", "confidence": 0.4511663019657135 },
        { "transcript": "alcohol", "confidence": 0.5986367464065552 },
        { "transcript": "Alcohol", "confidence": 0.6766515970230103 }
    ];

    let res = processTranscription(transcription);

    return Promise.resolve(res); */

    let data = new FormData();
    data.append("file", audio);
    data.append("language", googleLanguages[exos.language]);

    let init = {
        method: 'POST',
        body: data
    };

    return fetch("pages/serve_stt.php", init)
        .then(checkStatus)
        .then(getJSON)
        .then((data) => Promise.resolve(processTranscription(data)));

}


function processTranscription(transcript) {
    let res = {
        "transcript": "",
        "confidence": 0,
        "correct": null // indicates if it is the correct word
    }

    transcript.sort(function (a, b) {
        return b.confidence - a.confidence;
    });

    let mostLikely = transcript[0];

    // we always take the most likely to have the closest phonetic transcription, even when wrong
    // we remove whitespaces for the MAUs pipeline
    // TODO unfortunately we need to remove accents for the maus pipeline, see if there's a fix for that
    res.transcript = mostLikely.transcript.replace(/\s/g, '-').normalize("NFD").replace(/[\u0300-\u036f]/g, "");

    // Case 1, good pronunciatioon
    if (mostLikely.transcript.toLowerCase() == exos.current.name.toLowerCase()) {
        res.confidence = mostLikely.confidence;
        res.correct = true;
        return res;
    }

    // Case 2, mild pronunciation, correct word in list of possibilities but not at top of list
    for (const alternative of transcript) {
        if (alternative.transcript.toLowerCase() == exos.current.name.toLowerCase()) {
            res.confidence = alternative.confidence; // we get the confidence for the correct word
            res.correct = false;
            return res;
        }
    }

    // Case 3, bad pronunciation, word not in list of possibilities
    res.confidence = -1;
    res.correct = false;

    return res;
}


function updateScore(transcript, answerSyllables, userSyllables) {
    let percentageEl = document.getElementsByClassName('fc-percentage')[exos.currentInvNumber];
    let userEl = document.querySelectorAll(".fc-recording")[exos.currentInvNumber].querySelectorAll(".fc-syllab");
    let finalScore = 0;
    // TODO maybe not very efficient
    let computedScore = compareSyllables(answerSyllables, userSyllables, userEl);

    if (transcript.correct) {
        finalScore = scaleConfidence(transcript.confidence);
    } else if (transcript.confidence > 0) {
        finalScore = (scaleConfidence(transcript.confidence) + computedScore) / 2;
    } else {
        finalScore = computedScore;
    }

    percentageEl.innerHTML = finalScore + "%";
}

function compareSyllables(answerSyllables, userSyllables, userEl) {
    let minLength = Math.min(answerSyllables.length, userSyllables.length);
    let maxLength = Math.max(answerSyllables.length, userSyllables.length);

    let score = 0;
    for (let i = 0; i < minLength; i++) {
        if (answerSyllables[i] === userSyllables[i]) {
            score += 100 / maxLength;
            userEl[i].style.color = "green";
        } else {
            score += (stringSimilarity.compareTwoStrings(answerSyllables[i], userSyllables[i]) * 100) / maxLength;
            userEl[i].style.color = "red";
            console.log(answerSyllables[i] + " & " + userSyllables[i] + " = " + stringSimilarity.compareTwoStrings(answerSyllables[i], userSyllables[i]));
        }
    }

    if (userSyllables.length > answerSyllables.length) {
        let answerDiv = document.getElementsByClassName("fc-original")[exos.currentInvNumber];
        for (let i = minLength; i < maxLength; i++) {
            userEl[i].style.color = "red";
            // adding extra empty tag to have grid like display for the syllabs
            let tag = document.createElement("p");
            tag.classList.add("fc-syllab");
            answerDiv.appendChild(tag);
        }
    } else if (userSyllables.length < answerSyllables.length) {
        let recordingDiv = document.getElementsByClassName("fc-recording")[exos.currentInvNumber];
        for (let i = minLength; i < maxLength; i++) {
            // adding extra empty tag to have grid like display for the syllabs
            let tag = document.createElement("p");
            tag.classList.add("fc-syllab");
            recordingDiv.appendChild(tag);
        }
    }

    return Math.round(score);
}

function scaleConfidence(confidence) {
    // We only consider values between 0.3 and 0.9 (below is 0% and over is 100%)
    let score = Math.round(((confidence - 0.3) * 100) / 0.6);
    return score > 100 ? 100 : score;
}

function runPipeline(audio, name) {
    let data = new FormData();
    data.append("SIGNAL", audio, name + ".wav");
    data.append("LANGUAGE", BASLanguages[exos.language]);
    data.append("OUTFORMAT", "emuDB");
    //TODO see why ipa doesn't work
    //data.append("OUTSYMBOL", "ipa");
    //data.append("stress", "yes");
    data.append("PIPE", "G2P_MAUS_PHO2SYL");
    data.append("TEXT", new File([name], name + ".txt"));

    let init = {
        method: 'POST',
        body: data
    };

    return fetch("https://clarin.phonetik.uni-muenchen.de/BASWebServices/services/runPipeline", init)
        .then(checkStatus)
        .then(response => response.text())
        .then((data) => {
            console.log(data);
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(data, "text/xml");
            if (xmlDoc.getElementsByTagName("success")[0].childNodes[0].nodeValue == "true") {
                return Promise.resolve(xmlDoc.getElementsByTagName("downloadLink")[0].childNodes[0].nodeValue)
            } else {
                //console.log("error with the API");
                return Promise.reject(new Error("error with the API"))
            }
        })
        .then((link) => fetch(link))
        .then(checkStatus)
        .then(getJSON);
}

function blobToAudioRecord(audioParam, sampleRate) {
    let typedAnswerAudio = new AudioRecord(sampleRate);
    return audioParam.arrayBuffer()
        // decodeAudioData gives us access to getChannelData for the Float32Array representation
        .then(buffer => audioCtx.decodeAudioData(buffer))
        .then((decodedData) => {
            let float32Audio = decodedData.getChannelData(0);
            // the data is all in one big array and AudioRecord processes data in chunks of 4096, we need to split it
            let i = 0;
            let j = 4096;
            while (i < float32Audio.length) {
                const chunk = float32Audio.slice(i, j);
                typedAnswerAudio.push(chunk);
                i += 4096;
                j += 4096;
            }
            return Promise.resolve(typedAnswerAudio);
        });
}

function loadAnswerData() {
    let answerAudioBlob = null;
    let syllables = null;
    // We cannot get the blob with Audio API, so we need to fetch it
    return fetch(exos.current.soundpath)
        .then(checkStatus)
        .then(getBlob)
        .then((blob) => {
            answerAudioBlob = blob;
            return runPipeline(blob, exos.current.name);
        })
        .then((syll) => {
            syllables = syll;
            return blobToAudioRecord(answerAudioBlob, syllables.sampleRate);
        })
        .then((answerAudioRecord) => {
            loadSyllabes(answerAudioRecord, syllables, true);
            return Promise.resolve();
        });
}

function loadUserData(userAudio, transcription) {
    console.log(transcription);
    // add score modification
    return runPipeline(userAudio.getBlob(), transcription.transcript)
        .then((syllabes) => {
            loadSyllabes(userAudio, syllabes);
            return Promise.resolve();
        });
}

function loadData(dataJSON) {
    exos.exosTab = dataJSON;
    let i = 0;
    let container = document.getElementsByClassName("fc-cards-container")[0];
    for (exo in exos.exosTab) {
        exos.indexes.push(exo);
        if (i > 0) {
            container.prepend(exoHTML.cloneNode(true));
        }
        document.getElementsByClassName("fc-image fc-card-image")[0].src = exos.get(i).imagepath; // 0 because we prepend
        document.getElementsByClassName("fc-answer")[0].innerHTML = exos.get(i).name;
        exos.get(i).recordState = recordState.IDLE;
        i++;
    }
    exos.nbExos = i;
    document.getElementsByClassName("fc-exos-count")[0].innerHTML = "1/" + (exos.nbExos);

    // resetting arrow if needed
    let nextArrow = document.getElementsByClassName("fc-arrow")[1];
    if (exos.nbExos == 1) {
        nextArrow.onclick = "";
        nextArrow.classList.add("fc-inactive-arrow");
    } else {
        nextArrow.classList.remove("fc-inactive-arrow");
        nextArrow.onclick = nextExo;
    }

    // setting banner
    document.getElementsByClassName("fc-banner-title")[0].innerHTML = exos.level + " " + exos.language;

    console.log(exos.exosTab);
}

function nextExo() {
    if (exos.currentIndex + 1 == exos.nbExos) {
        console.log("finito");
    } else {
        document.getElementsByClassName("fc-main-card")[exos.currentInvNumber].classList.add("fc-move-left");
        exos.currentIndex++;
        document.getElementsByClassName("fc-exos-count")[0].innerHTML = (exos.currentIndex + 1) + "/" + (exos.nbExos);
        if (exos.currentIndex + 1 == exos.nbExos) {
            let nextArrow = document.getElementsByClassName("fc-arrow")[1];
            nextArrow.onclick = "";
            nextArrow.classList.add("fc-inactive-arrow");
        }
        if (exos.currentIndex == 1) {
            let prevArrow = document.getElementsByClassName("fc-arrow")[0];
            prevArrow.classList.remove("fc-inactive-arrow");
            prevArrow.onclick = previousExo;
        }
    }
}

function previousExo() {
    if (exos.currentIndex == 0) {
        console.log("we at the start yo");
    } else {
        exos.currentIndex--;
        document.getElementsByClassName("fc-exos-count")[0].innerHTML = (exos.currentIndex + 1) + "/" + (exos.nbExos);
        document.getElementsByClassName("fc-main-card")[exos.currentInvNumber].classList.remove("fc-move-left");
        if (exos.currentIndex == 0) {
            let prevArrow = document.getElementsByClassName("fc-arrow")[0];
            prevArrow.onclick = "";
            prevArrow.classList.add("fc-inactive-arrow");
        }
        if (exos.currentIndex + 1 == exos.nbExos - 1) {
            let nextArrow = document.getElementsByClassName("fc-arrow")[1];
            nextArrow.classList.remove("fc-inactive-arrow");
            nextArrow.onclick = nextExo;
        }
    }
}

function recordLoaded() {
    let circle = document.getElementsByClassName("fc-bottom-circle")[exos.currentInvNumber];
    circle.classList.remove("fc-no-micro");
    circle.classList.add("fc-retry");

    let loader = document.getElementsByClassName("fc-loader")[exos.currentInvNumber];
    loader.style.display = "none";

    let arrows = document.getElementsByClassName("fc-arrow");
    if (!previousLeftArrowClass) {
        arrows[0].classList.remove("fc-inactive-arrow");
    }
    if (!previousRightArrowClass) {
        arrows[1].classList.remove("fc-inactive-arrow");
    }
    arrows[0].onclick = previousExo;
    arrows[1].onclick = nextExo;

    exos.current.recordState = recordState.LOADED;
}

function clearSyllables() {
    let syllables = document.querySelectorAll(".fc-details")[exos.currentInvNumber].querySelectorAll(".fc-syllab");
    [...syllables].forEach(el => el.remove());
}

function record() {
    if (exos.current.recordState !== recordState.PROCESSING) {
        let circle = document.getElementsByClassName("fc-bottom-circle")[exos.currentInvNumber];
        switch (exos.current.recordState) {
            case recordState.IDLE:
                recorder.toggle();
                circle.classList.remove("fc-micro");
                circle.classList.add("fc-micro-recording");
                exos.current.recordState = recordState.RECORDING;
                let arrows = document.getElementsByClassName("fc-arrow");
                previousLeftArrowClass = arrows[0].classList.contains("fc-inactive-arrow");
                previousRightArrowClass = arrows[1].classList.contains("fc-inactive-arrow");
                [...arrows].forEach(el => {
                    el.onclick = "";
                    el.classList.add("fc-inactive-arrow");
                });
                break;
            case recordState.RECORDING:
                recorder.toggle();
                circle.classList.remove("fc-micro-recording");
                circle.classList.add("fc-no-micro");
                let loader = document.getElementsByClassName("fc-loader")[exos.currentInvNumber];
                loader.style.display = "inline-block";
                exos.current.recordState = recordState.PROCESSING;
                break;
            case recordState.LOADED:
                document.getElementsByClassName("fc-flip-card")[exos.currentInvNumber].classList.remove("fc-rotate");
                circle.classList.remove("fc-retry");
                circle.classList.add("fc-micro");
                clearSyllables();
                hideDetails();
                exos.current.recordState = recordState.IDLE;
                break;
        }
    }
}

function loadSyllabes(audioRecord, syllabes, isAnswer = false) {

    console.log(syllabes);

    let syllab = null;
    let duration = null;
    let start = null;
    let syllabesAudioTab = [];
    let syllabesTab = [];
    let className = null;
    let func = null;
    let i = 0;

    if (isAnswer) {
        className = "fc-original";
        func = function () {
            listenAnswerSyllabe(this.attributes.value.value);
        };
    } else {
        className = "fc-recording";
        func = function () {
            listenRecordSyllabe(this.attributes.value.value);
        };
    }

    let recordingDiv = document.getElementsByClassName(className)[exos.currentInvNumber];

    for (const item of syllabes.levels[3].items) {
        syllab = item.labels[0].value;
        start = item.sampleStart / syllabes.sampleRate;
        duration = item.sampleDur / syllabes.sampleRate;
        if (!syllab.includes("<")) {
            let tag = document.createElement("p");
            tag.appendChild(document.createTextNode(syllab));
            tag.classList.add("fc-syllab");
            tag.setAttribute("value", i + "");
            tag.onclick = func;
            recordingDiv.appendChild(tag);

            let temp = _.cloneDeep(audioRecord);
            temp.ltrim(start);
            temp.rtrim(audioRecord.getDuration() - (start + duration))

            syllabesAudioTab.push(temp);
            syllabesTab.push(syllab);
            i++;
        }
    }

    if (isAnswer) {
        // We prefer to have an Audio object to call onended later
        exos.current.answerAudio = {
            full: new Audio(exos.current.soundpath),
            syllables: syllabesAudioTab,
        }
        exos.current.answerSyllables = syllabesTab;
    } else {
        console.log(syllabesTab);
        exos.current.recordingAudio = {
            full: _.cloneDeep(audioRecord),
            syllables: syllabesAudioTab,
        }
        exos.current.recordingSyllables = syllabesTab;
        console.log(exos.current);
    }
}

function listenRecordSyllabe(i) {
    exos.current.recordingAudio.syllables[i].play();
}

function listenAnswerSyllabe(i) {
    exos.current.answerAudio.syllables[i].play();
}

function listenAnswer() {
    let audio = exos.current.answerAudio.full;
    audio.play();
    var gram = document.getElementsByClassName("fc-gramophone")[exos.currentInvNumber];
    gram.classList.remove("fc-inactive");
    gram.classList.add("fc-active");
    audio.onended = function () {
        gram.classList.remove("fc-active");
        gram.classList.add("fc-inactive");
    }
}

function listenRecording() {
    exos.current.recordingAudio.full.play();
}

function toggleDetails() {
    let percentage = document.getElementsByClassName("fc-percentage")[exos.currentInvNumber];
    let details = document.getElementsByClassName("fc-details")[exos.currentInvNumber];
    let loupe = document.getElementsByClassName("fc-loupe")[exos.currentInvNumber];
    if (window.getComputedStyle(details).display === "none") { // we need the computed value, because style only checks inline
        percentage.style.display = "none";
        details.style.display = "block";
        loupe.classList.remove("fc-inactive");
        loupe.classList.add("fc-active");
    } else {
        details.style.display = "none";
        percentage.style.display = "block";
        loupe.classList.remove("fc-active");
        loupe.classList.add("fc-inactive");
    }
}

function hideDetails() {
    let loupe = document.getElementsByClassName("fc-loupe")[exos.currentInvNumber];
    let percentage = document.getElementsByClassName("fc-percentage")[exos.currentInvNumber];
    let details = document.getElementsByClassName("fc-details")[exos.currentInvNumber];
    if (window.getComputedStyle(details).display !== "none") {
        details.style.display = "none";
        percentage.style.display = "block";
        loupe.classList.remove("fc-active");
        loupe.classList.add("fc-inactive");
    }
}