<?php
require(__DIR__ . '/../../../config.php');

$content = trim(file_get_contents("php://input"));
$params = json_decode($content, true);

global $DB;
$exos = [];
$query = "SELECT * FROM {flashcard_media} WHERE language='{$params["language"]}' AND level='{$params["level"]}' AND (flashcard='{$params["cmid"]}' OR flashcard IS NULL)";
$exos = $DB->get_records_sql($query);

//print_r($exos);

echo json_encode($exos);
