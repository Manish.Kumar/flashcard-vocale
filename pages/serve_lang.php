<?php
require(__DIR__ . '/../../../config.php');

$content = trim(file_get_contents("php://input"));
$params = json_decode($content, true);

global $DB;
$index = [];
$exos = [];
$query = "SELECT DISTINCT language FROM {flashcard_media} WHERE (flashcard='{$params['cmid']}' OR flashcard IS NULL)";
$exos = $DB->get_records_sql($query);

echo json_encode($exos);