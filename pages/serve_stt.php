<?php
require(__DIR__ . '/../../../config.php');

putenv('GOOGLE_APPLICATION_CREDENTIALS=<PATH_TO_CREDENTIALS>');

require_once('../vendor/autoload.php');

use Google\Cloud\Speech\V1\SpeechClient;
use Google\Cloud\Speech\V1\RecognitionAudio;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;

# The name of the audio file to transcribe

//$audioResource = fopen(__DIR__ . '/../pix/sounds/Krokodil.wav', 'r');
$content = file_get_contents($_FILES['file']['tmp_name']);
# set string as audio content
$audio = (new RecognitionAudio())
    ->setContent($content);

# The audio file's encoding, sample rate and language
$config = new RecognitionConfig([
    'encoding' => AudioEncoding::LINEAR16,
    'sample_rate_hertz' => 48000,
    'language_code' => $_POST['language'],
    'max_alternatives' => 10,
]);

# Instantiates a client
$client = new SpeechClient();

$processedResponse = [];
# Detects speech in the audio file
$response = $client->recognize($config, $audio);

# Print most likely transcription
foreach ($response->getResults() as $result) {
    //printf($result->getAlternatives()[1]->getTranscript());
    foreach ($result->getAlternatives() as $alternative) {

        $processedResponse[] = array('transcript' => $alternative->getTranscript(), 'confidence' => $alternative->getConfidence());
        //echo 'Transcript: ' . $alternative->getTranscript() . PHP_EOL . 'Confidence: ' . $alternative->getConfidence() . PHP_EOL;
    }
}

echo json_encode($processedResponse);

$client->close();
