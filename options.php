<?php
unset($op_languages);
global $op_languages;

unset($op_levels);
global $op_levels;

$op_languages = array('english', 'french', 'italian', 'spanish', 'german');

$op_levels = array('easy', 'medium', 'hard');