# Flashcard Vocale #

Développement d'un plugin moodle qui a pour but d'aider à l'apprentissage de plusieurs langues en tenant compte de la prononciation. Ce projet ajoute un type d'activité nommé "flashcard" que l'on peut ajouter dans une section d'un cours.

L'activité permet à un utilisateur de tester sa prononciation de mots dans un certain langage. Une personne peut donc enregistrer sa prononciation du mot sur la flashcard et l'activité découpera ce mot en syllabes afin de donner une note en comparant à une prononciation correcte du mot en question. La personne peut donc écouter chacune des syllabes et tenter d'améliorer son score.

![Icône de l'activité dans un cours](screenshots/affichage_cours.png)
![Menu de l'activité](screenshots/menu.png)
![Sélection de difficulté](screenshots/niveaux.png)
![Exemple de flashcard](screenshots/flashcard.png)
![Exemple de score](screenshots/resultats.png)
![Découpage en syllabes](screenshots/details.png)
![Ajout ou suppression de flashcards](screenshots/modification.png)

## Installation ##

Pour installer le plugin il suffit d'abord de cloner ou de télécharger ce projet dans le dossier `/mod` de votre répertoire moodle.

Ensuite, il faudra upgrade moodle soit en se loggant en tant qu'administrateur, soit en tapant la commande suivante depuis la racine de votre moodle : `php admin\cli\upgrade.php` (si votre moodle tourne sur XAMPP, faites-le depuis la console de XAMPP).

Ce plugin nécessite également une clé valide pour utiliser l'API Speech-to-Text de Google. Le chemin vers le fichier contenant les credentials doit être indiqué dans le fichier `pages/serve_stt.php` à la place de `<PATH_TO_CREDENTIALS>`.

## Conseils pour le développement ##

+ On peut créer des cours de test pour tester le plugin en allant simplement sous `Administration du site > Développement > Créer des cours de test`.

+ Il pourrait être utile d'activer `Afficher les informations de débogage` sous `Administration du site > Développement > Débogage` pour afficher les erreurs directement sur la page.

+ Après avoir fait des modifications sur certains fichiers du plugin, comme le fichier `install.xml` par exemple, il vous faudra désinstaller et réinstaller le plugin pour que les changements prennent effet. 

    Voici comment le faire depuis la ligne de commande : 
  1. `php admin\cli\uninstall_plugins.php --plugins=mod_flashcard --run`
  2. `php admin\cli\upgrade.php` et confirmer

+ Les feuilles de style (CSS) sont mises en cache par défaut et les changements aux fichiers CSS ne seront donc pas visibles. Pour y remédier, vous pouvez aller sous `Administration du site > Présentation > Thèmes > Réglages thème` et activer `Mode concepteur de thème` (themedesignermode), mais le site sera plus lent en contrepartie. Sinon, vous pouvez purger les caches manuellement dans les paramètres ou en utilisant `php admin\cli\purge_caches.php` après chaque modification.

+ La base du squelette du plugin a été généré avec un plugin externe : [Moodle plugin skeleton generator](https://moodle.org/plugins/tool_pluginskel) 
  
## License ##

2021 Manish Kumar & Sara Cousin

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
